import Vue from 'vue';
import Router from 'vue-router';
import HelloWorld from '@/components/HelloWorld';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: '/borisbirthday/',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld,
    },
    {
      path: '/level2',
      name: 'level2',
      component: () => import('../components/level2/veiwer'),
    },
    {
      path: '/level3',
      name: 'level3',
      component: () => import('../components/level3'),
    },
    {
      path: '/level4',
      name: 'level4',
      component: () => import('../components/level4'),
    },
  ],
});

router.beforeEach((to, from, next) => {
  next();
});

export default router;
