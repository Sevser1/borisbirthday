// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import '@babel/polyfill';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import App from './App';
import router from './router';
import store from './store';
import globalMixin from './mixins/global';
import vuexRouter from './store/router';

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

Vue.config.productionTip = false;
Vue.mixin(globalMixin);

/* eslint-disable no-new */
const VueInstance = new Vue({
  router,
  store,
  render: h => h(App),
});

store.registerModule('router', vuexRouter);
VueInstance.$mount('#app');
