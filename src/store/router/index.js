import router from '@/router';

export default {
  namespaced: true,
  actions: {
    push: (store, payload) => router.push(payload),
    redirectToAccount: () => {
      if (router.currentRoute.meta.authRequired) {
        router.push({
          name: 'login',
          query: {
            redirect: router.currentRoute.fullPath,
          },
        });
      }
    },
  },
};
